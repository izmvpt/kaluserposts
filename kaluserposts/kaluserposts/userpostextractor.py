#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from requests import get
from time import sleep
import os, sys

def soup(url):
	soup = BeautifulSoup(get(url).content, 'lxml')
	return soup

def content(soup, selected_user):
	'''Return list of user comment contents'''
	content = []
	count = 0
	for article in soup.find_all('article'):
		try:
			# Името на потребителя от линка към профила
			username = article.aside.h3.strong.a.get_text()
		except:
			# Гостите нямат линк
			username = 'Гост'
		if username == selected_user:
			count += 1
			date = article.find('div', class_="ipsComment_meta")
			post = article.find('div', class_='ipsContained')
			content.append('<hr>' + str(date) + str(post) + '<hr>')
	
	if count != 0:
		print('Брой открити постове: {}'.format(count))
		
	return content

def main():
	print('Събира постове в html файл (/tmp/<username>.html)\n'
        'на определен потребител от тема във форумите на Калдата.\n')

	# Начална страница на темата, в която да търси
	base_url = input('Постави линк към темата: ')
	# Изчисти ненужни параметри след последната наклонена черта
	base_url = '/'.join(base_url.split('/')[0:-1]) 

	# За чий постове да търси	
	selected_user = input('Постави името на потребителя: ')

	if base_url == '' or selected_user == '':
		print('Празни стойности не са приемливи.')
		sys.exit(1)

	while True:
		directory = input('Постави съществуваща директория\n'
							'където да запамети файла: ')
		if os.path.isdir(directory) == False:
			print('Не съществува такава директория.\n')
		else:
			break
			
	html_file = os.path.join(directory, (selected_user + '.html'))
	print('Файл за съхранение: ' + html_file)
	start_page = soup(base_url)
	pages = start_page.find('ul', class_="ipsPagination")['data-pages']

	f = open(html_file, "a")
	f.write('<head><meta charset="UTF-8"></head>\n')

	for i in range(1, int(pages)+1):
		if i == 1:
			print('Проверка на първата страница...')
			content_list = content(start_page, selected_user)
		else:
			print('Заявка за страница №: ' + str(i))
			page_url = base_url + "/?page=" + str(i)
			content_list = content(soup(page_url), selected_user)
		f.write('\n'.join(content_list))
		sleep(5)
		
	f.close()

if __name__ == "__main__":
	main()
