from setuptools import setup

setup(name='kaluserposts',
      version='0.1',
      description='Stores one kaldata forums user posts in html file forom one thread.',
      url='http://github.com/',
      author='Flying Circus',
      author_email='flyingcircus@example.com',
      license='MIT',
      packages=['funniest'],
      zip_safe=False)
